/*la base des personnages */

class Personnage{
  /*stats en commun joueur, monstre*/
  constructor(nom,attaque,defence,vitesse,xp){
    this.nom = nom;
    this.attaque = attaque;
    this.defence = defence;
    this.vitesse = vitesse;
    this.xp = xp;
  }

}

class Joueur {

  /*stats joueur*/
  constructor(nom,attaque,defence,vitesse,xp,fain,soif,bonneur,genre,img){
    this.nom = nom;
    this.attaque = attaque;
    this.defence = defence;
    this.vitesse = vitesse;
    this.xp = xp;
    this.fain = fain;
    this.soif = soif;
    this.bonneur = bonneur;
    this.genre = genre;
    this.img = img;
  }
 
}


